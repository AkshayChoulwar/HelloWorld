package com.example.akshaychoulwar.myapplication;

/**
 * Created by akshaychoulwar on 5/21/2017.
 */


public class Tablet {

    //private variables
    int _id;
    String _name;
    int _quantity;

    // Empty constructor
    public Tablet(){

    }
    // constructor
    public Tablet(int id, String name, int _quantity){
        this._id = id;
        this._name = name;
        this._quantity = _quantity;
    }

    // constructor
    public Tablet(String name, int _quantity){
        this._name = name;
        this._quantity = _quantity;
    }
    // getting ID
    public int getID(){
        return this._id;
    }

    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting name
    public String getName(){
        return this._name;
    }

    // setting name
    public void setName(String name){
        this._name = name;
    }

    // getting phone number
    public int getQuantity(){
        return this._quantity;
    }

    // setting phone number
    public void setQuantity(int quantity){
        this._quantity = quantity;
    }
}
