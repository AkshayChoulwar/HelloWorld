package com.example.akshaychoulwar.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by akshaychoulwar on 5/21/2017.
 */

public class SplashScreen extends Activity {
    private static final long DELAY=5000;
    private boolean scheduled=false;
    private Timer splashTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);

        splashTimer=new Timer();


            splashTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    SplashScreen.this.finish();
                    startActivity(new Intent(SplashScreen.this,MainActivity.class));

                }
            },DELAY);
            scheduled=true;



    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if(scheduled){
            splashTimer.cancel();
            splashTimer.purge();
        }
    }

}

