package com.example.akshaychoulwar.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by akshaychoulwar on 5/22/2017.
 */

public class TabletAdapter extends ArrayAdapter<Tablet> {

    public TabletAdapter(Activity context, ArrayList<Tablet> tabletArrayList){
        super(context,0,tabletArrayList);
    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent)
    {
        View listItemView= convertview;
        if(listItemView==null){
            listItemView= LayoutInflater.from(getContext()).inflate(R.layout.data,parent,false);
        }

        Tablet currentTablet=getItem(position);


        TextView nameTextView = (TextView) listItemView.findViewById(R.id.getid);
        // Get the version name from the current AndroidFlavor object and
        // set this text on the name TextView
        int i= currentTablet.getID();
        String j=Integer.toString(i);
        nameTextView.setText(j);


        // Find the TextView in the list_item.xml layout with the ID version_number
        TextView numberTextView = (TextView) listItemView.findViewById(R.id.getname);
        // Get the version number from the current AndroidFlavor object and
        // set this text on the number TextView
        numberTextView.setText(currentTablet.getName());

        TextView qty=(TextView) listItemView.findViewById(R.id.getqty);
        int test=currentTablet.getQuantity();
        if(test<10){
            qty.setText(Integer.toString(currentTablet.getQuantity()));
            qty.setTextColor(Color.RED);
            //MainActivity a=new MainActivity();
            //String phone=a.getPhone();
            //
        }
        else {

            qty.setText(Integer.toString(currentTablet.getQuantity()));
            qty.setTextColor(Color.parseColor("#4CAF50"));
        }

        return listItemView;
    }
}
