package com.example.akshaychoulwar.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by akshaychoulwar on 5/21/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    //all static varialbles
    private static final int DATABASE_VERSION=1;

    //database name
    private static final String DATABASE_NAME="contactsManager";
    //contacts table name
    private static final String TABLE_CONTACTS="contacts";

    //contacts table column name
    private static final String KEY_ID="id";
    private static final String KEY_NAME="name";
    private static final String KEY_PH_NO="phone_number";

    public DatabaseHandler(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    //creaating tables
    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_CONTACTS_TABLE="CREATE TABLE "+TABLE_CONTACTS+"("+KEY_ID+ " INTEGER PRIMARY KEY,"+ KEY_NAME + " TEXT,"+KEY_PH_NO+ " INTEGER"+")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
        //Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CONTACTS);

        onCreate(db);
    }
    /* crud operations */
    //Adding new contact
    public void addContact(Tablet tablet){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_NAME,tablet.getName());
        values.put(KEY_PH_NO,tablet.getQuantity());
        //inserting row
        db.insert(TABLE_CONTACTS,null,values);
        db.close();//closing database connection

    }
    //updating single contact
    public int updateContact(Tablet tablet){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_NAME,tablet.getName());
        values.put(KEY_PH_NO,tablet.getQuantity());

        //updating row
        return db.update(TABLE_CONTACTS,values,KEY_ID + "= ?",new String[]{String.valueOf(tablet.getID())});
    }
    //getting all contacts
    public ArrayList<Tablet> getAllContacts(){


        ArrayList<Tablet> contactList=new ArrayList<Tablet>();
        //select all query
        String selectQuery="SELECT * FROM "+TABLE_CONTACTS;

        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(selectQuery,null);
        //looping through all rows and adding to list
        if(cursor.moveToFirst()){
            do{
                Tablet tablet=new Tablet();
                tablet.setID(Integer.parseInt(cursor.getString(0)));
                tablet.setName(cursor.getString(1));
                tablet.setQuantity(Integer.parseInt(cursor.getString(2)));
                //adding contact list to list
                contactList.add(tablet);

            }while (cursor.moveToNext());
        }
        return contactList;
    }
    public void clearall(){
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_CONTACTS,null,null);
    }
    public void updateall(){
        String query="UPDATE "+TABLE_CONTACTS+" SET "+KEY_PH_NO+" = "+KEY_PH_NO+"-1";

        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL(query);
    }


}