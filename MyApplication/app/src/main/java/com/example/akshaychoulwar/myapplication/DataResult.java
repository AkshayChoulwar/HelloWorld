package com.example.akshaychoulwar.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by akshaychoulwar on 5/22/2017.
 */

public class DataResult extends AppCompatActivity {

    //TextView result;
    @Override
    protected void onCreate(Bundle savedInstanceState)
{
    super.onCreate(savedInstanceState);
    setContentView(R.layout.viewcontents_layout);
    DatabaseHandler db=new DatabaseHandler(this);
    ListView result=(ListView) findViewById(R.id.try2);
    ActionBar actionBar=getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);

    ArrayList<Tablet> contacts = db.getAllContacts();

    TabletAdapter tabletAdapter=new TabletAdapter(this,contacts);

    ListAdapter listAdapter= new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,contacts);

    result.setAdapter(tabletAdapter);
    //for (Tablet cn : contacts) {
    //String log = "Id: "+cn.getID()+" ,Name: " + cn.getName() + " ,Quantity: " + cn.getQuantity();
     //result.append(log);
    //}



}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        startActivityForResult(intent,0);
        return true;
    }

}
