package com.example.akshaychoulwar.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by akshaychoulwar on 5/31/2017.
 */

public class Hello extends AppCompatActivity {

    Button hey;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.hello);
        hey=(Button) findViewById(R.id.next);

        hey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Hello.this,MainActivity.class);
                startActivity(intent);
            }
        });



    }
}
