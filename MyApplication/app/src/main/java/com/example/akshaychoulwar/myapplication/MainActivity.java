package com.example.akshaychoulwar.myapplication;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    EditText name;
    TextView welcome;
    EditText qty;
    Button submit,show,clear,set,update,animate;
    private FloatingActionButton btnShow,buttonSms;
    private PendingIntent pint;
    private AlarmManager alarmManager;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.testing);


        final DatabaseHandler db = new DatabaseHandler(this);
        name=(EditText) findViewById(R.id.tablet);
        qty=(EditText) findViewById(R.id.quantity);
        submit=(Button) findViewById(R.id.submit);
        btnShow=(FloatingActionButton) findViewById(R.id.menu_item1);
        buttonSms=(FloatingActionButton) findViewById(R.id.menu_item);

        welcome=(TextView) findViewById(R.id.welcome);
        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(this);
        final String phonenumber=pref.getString("example_mobile","0000000000");
        String disp=pref.getString("example_text","");
        if(disp.length()==0){
            welcome.setText("Welcome to MedEase!");
        }
        else{
            welcome.setText("Welcome "+disp);
        }


        if(notificationstatus())
        {

            Intent alarmintent=new Intent(this,AlarmReceiver.class);
            pint=PendingIntent.getBroadcast(this,0,alarmintent,0);
            alarmManager=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
            Calendar firingcalendar=Calendar.getInstance();
            Calendar currentcal=Calendar.getInstance();
            Calendar second=Calendar.getInstance();
            second.set(Calendar.HOUR_OF_DAY,8);
            second.set(Calendar.MINUTE,0);
            second.set(Calendar.SECOND,0);

            firingcalendar.set(Calendar.HOUR_OF_DAY,23);
            firingcalendar.set(Calendar.MINUTE,0);
            firingcalendar.set(Calendar.SECOND,0);

            long intendedTime=firingcalendar.getTimeInMillis();
            long secondintendedTime=second.getTimeInMillis();
            long currentTime=currentcal.getTimeInMillis();
            if(intendedTime>=currentTime){
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,intendedTime,AlarmManager.INTERVAL_DAY,pint);
            }
            else{
                firingcalendar.add(Calendar.DAY_OF_MONTH,1);
                intendedTime=firingcalendar.getTimeInMillis();
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,intendedTime,AlarmManager.INTERVAL_DAY,pint);
            }
            if(secondintendedTime>=currentTime){
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,secondintendedTime,AlarmManager.INTERVAL_DAY,pint);
            }
            else{
                second.add(Calendar.DAY_OF_MONTH,1);
                secondintendedTime=firingcalendar.getTimeInMillis();
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,secondintendedTime,AlarmManager.INTERVAL_DAY,pint);
            }
            db.updateall();

        }
        else {
            Intent alarmintent=new Intent(this,AlarmReceiver.class);
            pint=PendingIntent.getBroadcast(this,0,alarmintent,0);
            alarmManager=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pint);
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //inserting tablet information
                String one=name.getText().toString();
                String two=qty.getText().toString();
                int qua;
                if(two.length()!=0)
                    qua=Integer.parseInt(qty.getText().toString());
                else
                    qua=0;
                if(one.length()!=0)
                {
                    db.addContact(new Tablet(name.getText().toString(),qua));
                    Toast toast= Toast.makeText(getApplicationContext(),"Added Successfully",Toast.LENGTH_SHORT);
                    toast.show();
                }
                else{
                    Toast toast= Toast.makeText(getApplicationContext(),"Enter Valid Detalis",Toast.LENGTH_SHORT);
                    toast.show();
                }
                name.setText("");
                qty.setText("");
            }
        });




        buttonSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+phonenumber));
                    intent.putExtra("sms_body","Hurry Up! Add Tablets in shelf");
                    startActivity(intent);

                }
                catch(Exception ee)
                {
                    Toast.makeText(getApplicationContext(),"Message is not sent",Toast.LENGTH_SHORT).show();
                }
            }
        });



        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,DataResult.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }
    @Override

    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()){
            case R.id.action_setting:
                Intent intent=new Intent(MainActivity.this,SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_clear:
                DatabaseHandler db = new DatabaseHandler(this);
                db.clearall();
                Toast.makeText(getApplicationContext(),"Cleared Shelf",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public boolean notificationstatus()
    {
        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(this);
        return pref.getBoolean("notifications_new_message",true);
    }


}
