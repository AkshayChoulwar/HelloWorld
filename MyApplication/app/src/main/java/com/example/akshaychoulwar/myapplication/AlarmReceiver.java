package com.example.akshaychoulwar.myapplication;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;

/**
 * Created by akshaychoulwar on 5/25/2017.
 */

public class AlarmReceiver extends BroadcastReceiver {
   DatabaseHandler db;
    @Override
    public void onReceive(Context arg0, Intent arg1){


        android.support.v4.app.NotificationCompat.Builder mBuilder=
                new NotificationCompat.Builder(arg0)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("MedEase")
                        .setContentText("Time to take your tablets!!");


        Uri alarmsound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmsound);
        Intent intent=new Intent(arg0,DataResult.class);

        TaskStackBuilder taskStackBuilder=TaskStackBuilder.create(arg0);
        taskStackBuilder.addParentStack(DataResult.class);

        taskStackBuilder.addNextIntent(intent);

        PendingIntent pendingIntent=
                taskStackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationManager=(NotificationManager) arg0.getSystemService(Context.NOTIFICATION_SERVICE);
        int mid=5;
        notificationManager.notify(mid,mBuilder.build());

    }
}
